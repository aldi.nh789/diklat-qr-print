const axios = require('axios')
const debug = require('debug')('diklat:print')
const express = require('express')
const fs = require('fs')
const { exec } = require('child_process')

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(express.static('public'))
app.post('/print-qr', async (req, res) => {
	try {
		const key = Math.random()
		const pdfbox = 'printer/pdfbox.jar'
		const biodata = `printer/bio-${key}.pdf`

		const executor = (file) => `java -jar "${pdfbox}" PrintPDF "${file}" -silentPrint`

		const pdfBio = fs.createWriteStream(biodata)
		const pdfUrl = req.body.data
		const pdfBioStream = await axios({
			url: pdfUrl,
			method: 'GET',
			responseType: 'stream'
		})

		pdfBioStream.data.pipe(pdfBio)

		pdfBio.on('finish', () => {
			exec(executor(biodata), (err, stdout, stderr) => {
				if (err) {
					debug(err)
					res.json({status: 'failed'})
				} else {
					debug(`stdout : ${ stdout }`)
			    		debug(`stderr : ${ stderr }`)
					res.json({status: 'success'})
				}
			})
		})
	} catch(error) {
		debug(error)
		res.json({status: 'failed', error: error.message})
	}
})
app.use(notFound)
app.use(onError)

const server = app.listen(process.env.PORT || 3000, function() {
  debug('Server Berjalan pada port : %d', server.address().port)
})

function notFound(req, res, next) {
  res.status(404).end('Halaman Tidak Ditemukan')
}
function onError(err, req, res, next) {
  debug(err)
  res.status(500).end('Internal Server Error')
}
